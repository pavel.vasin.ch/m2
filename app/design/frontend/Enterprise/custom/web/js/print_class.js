jQuery( document ).on( "ready", function () {
    jQuery( "*" ).on( "click", function() {
        let curClass = jQuery( this ).attr( "class" );
        if ( typeof curClass !== 'undefined' ) {
            let curClassClear = curClass.replace(/\n/g, "").trim();
            console.log( "Element class is '", curClassClear, "'" );
        }
    });
});
<?php
/**
 * Enterprise/custom theme registration
 *
 * @category Enterprise
 * @package Enterprise_custom
 * @copyright Copyright (C) 2018 CyberHULL (www.cyberhull.com)
 * @author Pavel Vasin <pavel.vasin@cyberhull.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Enterprise/custom',
    __DIR__
);

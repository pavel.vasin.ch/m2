<?php
/**
 * Custom/custom theme registration
 *
 * @category Custom
 * @package Custom_custom
 * @copyright Copyright (C) 2018 CyberHULL (www.cyberhull.com)
 * @author Pavel Vasin <pavel.vasin@cyberhull.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Custom/custom',
    __DIR__
);

<?php
/**
 * Custom/custom2 theme registration
 *
 * @category Custom
 * @package Custom_custom2
 * @copyright Copyright (C) 2018 CyberHULL (www.cyberhull.com)
 * @author Pavel Vasin <pavel.vasin@cyberhull.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Custom/custom2',
    __DIR__
);

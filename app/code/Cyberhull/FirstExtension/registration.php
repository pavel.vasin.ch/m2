<?php
/**
 * Module Cyberhull_FirstExtension registration
 *
 * @category Cyberhull
 * @package Cyberhull_FirstExtension
 * @copyright Copyright (C) 2018 CyberHULL (www.cyberhull.com)
 * @author Pavel Vasin <pavel.vasin@cyberhull.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cyberhull_FirstExtension',
    __DIR__
);

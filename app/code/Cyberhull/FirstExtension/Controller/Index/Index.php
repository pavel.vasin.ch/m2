<?php
/**
 * Cyberhull_FirstExtension
 *
 * @category Cyberhull
 * @package Cyberhull_FirstExtension
 * @copyright Copyright (C) 2018 CyberHULL (www.cyberhull.com)
 * @author Pavel Vasin <pavel.vasin@cyberhull.com>
 */

/**
 * Main module controller
 *
 * @category Cyberhull
 * @package Cyberhull_FirstExtension
 */
namespace Cyberhull\FirstExtension\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute() {
        return $this->_pageFactory->create();
    }

}
